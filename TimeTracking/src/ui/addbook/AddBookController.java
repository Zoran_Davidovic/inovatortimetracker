package ui.addbook;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class AddBookController implements Initializable {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private JFXTextField new_sifra;
    @FXML
    private JFXComboBox<?> new_projekat;
    @FXML
    private JFXDatePicker new_datum;
    @FXML
    private JFXTextField new_sati;
    @FXML
    private JFXTextField new_minuti;
    @FXML
    private JFXTextField new_satnica;
    @FXML
    private JFXButton save;
    @FXML
    private JFXButton cancel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void cancel(ActionEvent event) {
    }
    
}

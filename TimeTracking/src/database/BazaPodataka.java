package database;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import modeli.Projekat;
import modeli.Zadatak;


public final class BazaPodataka {
    
    private static BazaPodataka baza;
    
    private static Connection conn = null;
    private static Statement stmt = null;
    private static PreparedStatement pstmt = null;
    private static ResultSet rs = null;

    private BazaPodataka() {
        this.kreirajKonekciju();
    }
    
    public static BazaPodataka getInstance(){
        if(baza == null){
            baza = new BazaPodataka();
        }
        return baza;
    }
    
    private void kreirajKonekciju(){
        try{
            //Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:data.db");
            /* setup tabela */
            this.kreirajProjekatTabelu();
            this.kreirajZadatakTabelu();
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
    private void kreirajTabelu(String nazivTabele, String upit){
        try{
            stmt = conn.createStatement();
            DatabaseMetaData dbmd = conn.getMetaData();
            rs = dbmd.getTables(null, null, nazivTabele.toLowerCase(), null);
            if(!rs.next()){
                stmt.execute(upit);
            }
        }
        catch(SQLException ex){
            System.err.println(ex.getMessage() + " ...kreiranje baze.");
        }
        finally{
            try {
                stmt.clearBatch();
                stmt.close();
                rs.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /************************************************************************/
    /*************************** KREIRANJE TABELA ***************************/
    /************************************************************************/
    
    private void kreirajProjekatTabelu(){
        String upit = "CREATE TABLE projekat (" +
            "'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "'naziv' TEXT NOT NULL UNIQUE" +
            ");";
        kreirajTabelu("projekat", upit);
    }
    
    private void kreirajZadatakTabelu(){
        String upit = "CREATE TABLE zadatak (" +
            "'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "'sifra' TEXT NOT NULL, " +
            "'sati' INTEGER DEFAULT 0, " +
            "'minuti' INTEGER DEFAULT 0, " +
            "'satnica' DOUBLE NOT NULL DEFAULT 0, " +
            "'datum' DATE NOT NULL DEFAULT CURRENT_DATE, " +
            "'projekat' TEXT NOT NULL, " +
            "CONSTRAINT 'projekat_zadatak' FOREIGN KEY ('projekat') REFERENCES 'projekat' ('naziv') ON DELETE CASCADE ON UPDATE CASCADE" +
            ");";
        kreirajTabelu("zadatak", upit);
    }
        
    /************************************************************************/
    /******************************* PROJEKTI *******************************/
    /************************************************************************/
    public boolean projekatPostoji(String projekat){
        String upit = "SELECT Count(id) AS broj FROM projekat WHERE naziv = '"+projekat+"'";
        int broj = 0;
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(upit);
            while(rs.next()){
                broj = rs.getInt("broj");
            }
            return broj>0;
        }
        catch(SQLException e){
            e.printStackTrace();
            return broj>0;
        }
        finally{
            try {
                rs.close();
                stmt.clearBatch();
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public boolean kreirajProjekat(String projekat){
        String upit = "INSERT INTO projekat (naziv) VALUES (?)";
        try {
            pstmt = conn.prepareStatement(upit);
            pstmt.setString(1, projekat);
            pstmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        finally{
            try {
                pstmt.clearParameters();
                pstmt.clearBatch();
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void vratiSveProjekte(ObservableList<Projekat> list){
        String upit = "SELECT * FROM projekat";
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(upit);
            while(rs.next()){
                String naziv = rs.getString("naziv");
                
                Projekat p = new Projekat(naziv);
                list.add(p);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try {
                rs.close();
                stmt.clearBatch();
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /*public void vratiBrojZadatakaPoProjektu(ObservableList<PieChart.Data> datalist, ObservableList<Projekat> listaProjekata){
        String upit = "SELECT projekat, count(*) as NUM FROM zadatak GROUP BY projekat";
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(upit);
            while(rs.next()){
                String projekat = rs.getString("projekat");
                int num = rs.getInt("NUM");
                datalist.add(new PieChart.Data(projekat + " (" + num + ")", num));
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try {
                rs.close();
                stmt.clearBatch();
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }*/
    
    public void vratiBrojZadatakaPoProjektu(ObservableList<PieChart.Data> datalist, ObservableList<Projekat> listaProjekata){
        datalist.removeAll(datalist);
        String upit = "SELECT count(id) as count FROM zadatak WHERE zadatak.projekat = ?";
        int count = 0;
        try{
            for(Projekat p : listaProjekata){
                pstmt = conn.prepareStatement(upit);
                pstmt.setString(1, p.getNaziv());
                rs = pstmt.executeQuery();
                while(rs.next()){
                    count = rs.getInt("count");
                }
                datalist.add(new PieChart.Data(p.getNaziv() + " (" + count + ")", count));
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try {
                rs.close();
                stmt.clearBatch();
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /************************************************************************/
    /******************************** ZADACI ********************************/
    /************************************************************************/
    private void vratiZadatke(String upit, ObservableList<Zadatak> list){
        list.removeAll(list);
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(upit);
            while(rs.next()){
                String sifra = rs.getString("sifra");
                int sati = rs.getInt("sati");
                int minuti = rs.getInt("minuti");
                double satnica = rs.getDouble("satnica");
                String datum = rs.getString("datum");
                String projekat = rs.getString("projekat");
                
                Zadatak member = new Zadatak(sifra, sati, minuti, satnica, datum, projekat);
                list.add(member);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try {
                rs.close();
                stmt.clearBatch();
                stmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void vratiSveZadatke(ObservableList<Zadatak> list){
        //Upit vraca sve zadatke grupisane po šifri
        String upitBezGrupisanja = "SELECT "
            + "sifra, sati, minuti, satnica, "
            + "datum, projekat "
            + "FROM zadatak "
            + "ORDER BY datum ASC";
        vratiZadatke(upitBezGrupisanja, list);
    }
    
    public void filtrirajZadatke(String upit, ObservableList<Zadatak> list){
        vratiZadatke(upit, list);
    }
    
    public boolean dodajZadatak(String sifra, String projekat, String datum, 
            int sati, int minuti, double satnica){
        String upit = "INSERT INTO zadatak "
                + "(sifra, sati, minuti, satnica, datum, projekat) "
                + "VALUES "
                + "(?,?,?,?,?,?)";
        try{
            pstmt = conn.prepareStatement(upit);
            pstmt.setString(1, sifra);
            pstmt.setInt(2, sati);
            pstmt.setInt(3, minuti);
            pstmt.setDouble(4, satnica);
            pstmt.setString(5, datum);
            pstmt.setString(6, projekat);
            pstmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        finally{
            try {
                pstmt.clearParameters();
                pstmt.clearBatch();
                pstmt.close();
            } catch (SQLException ex) {
                Logger.getLogger(BazaPodataka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

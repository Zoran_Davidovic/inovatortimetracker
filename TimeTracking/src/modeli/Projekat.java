package modeli;

import javafx.beans.property.SimpleStringProperty;

public class Projekat {
    
    private SimpleStringProperty naziv;

    public Projekat() {
    }

    public Projekat(String naziv) {
        this.naziv = new SimpleStringProperty(naziv);
    }

    public String getNaziv() {
        return naziv.get();
    }

    @Override
    public String toString() {
        return naziv.get();
    }
    
    
}

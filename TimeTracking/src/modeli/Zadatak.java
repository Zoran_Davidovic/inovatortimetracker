package modeli;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Zadatak {
    
    private SimpleStringProperty sifra;
    private SimpleIntegerProperty sati;
    private SimpleIntegerProperty minuti;
    private SimpleDoubleProperty satnica;
    private SimpleStringProperty datum;
    private SimpleStringProperty projekat;
    private SimpleStringProperty vreme;
    private SimpleDoubleProperty zarada;

    public Zadatak() {
    }

    public Zadatak(String sifra, int sati, int minuti, double satnica, String datum, String projekat) {
        this.sifra = new SimpleStringProperty(sifra);
        this.sati = new SimpleIntegerProperty(sati);
        this.minuti = new SimpleIntegerProperty(minuti);
        this.satnica = new SimpleDoubleProperty(satnica);
        this.datum = new SimpleStringProperty(datum);
        this.projekat = new SimpleStringProperty(projekat);
        this.vreme = new SimpleStringProperty(izracunajVreme(sati, minuti));
        this.zarada = new SimpleDoubleProperty(izracunajZaradu(sati, minuti, satnica));
    }

    public String getSifra() {
        return sifra.get();
    }

    public int getSati() {
        return sati.get();
    }

    public int getMinuti() {
        return minuti.get();
    }

    public double getSatnica() {
        return satnica.get();
    }

    public String getDatum() {
        return datum.get();
    }

    public String getProjekat() {
        return projekat.get();
    }

    public String getVreme() {
        return vreme.get();
    }

    public double getZarada() {
        return zarada.get();
    }
    
    private String izracunajVreme(int sati, int minuti){
        if(minuti >= 60){
            sati += (minuti/60);
            minuti = (minuti%60);
        }
        return sati + "h" + minuti + "m";
    }
    
    private double izracunajZaradu(int sati, int minuti, double satnica){     
        double dSati = sati;
        double dMinuti = minuti;
        
        double ostatak = dMinuti / 60;
        
        double utrosenoVreme = dSati + ostatak;
        
        double zaradjenoEvra = utrosenoVreme * satnica;
        zaradjenoEvra *= 100;
        zaradjenoEvra = Math.round(zaradjenoEvra);
        zaradjenoEvra /= 100;
        
        return zaradjenoEvra;
    }
}

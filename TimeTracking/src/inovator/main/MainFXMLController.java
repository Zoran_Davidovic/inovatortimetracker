package inovator.main;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import com.jfoenix.effects.JFXDepthManager;
import database.BazaPodataka;
import inovator.zadatak.ZadatakController;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import modeli.Projekat;
import modeli.Zadatak;
import org.controlsfx.control.Notifications;

public class MainFXMLController implements Initializable {

    BazaPodataka bazaPodataka;
    Stage stage;
    
    ObservableList<Zadatak> listaZadataka = FXCollections.observableArrayList();
    ObservableList<Projekat> listaProjekata = FXCollections.observableArrayList();
    ObservableList<String> stringListaProjekata = FXCollections.observableArrayList();
    ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
    
    @FXML
    private TableView<Zadatak> tabela;
    @FXML
    private TableColumn<Zadatak, String> col_sifra;
    @FXML
    private TableColumn<Zadatak, String> col_projekat;
    @FXML
    private TableColumn<Zadatak, String> col_datum;
    @FXML
    private TableColumn<Zadatak, String> col_vreme;
    @FXML
    private TableColumn<Zadatak, Double> col_satnica;
    @FXML
    private TableColumn<Zadatak, Double> col_zarada;
    @FXML
    private JFXTextField tf_pretraga;
    @FXML
    private PieChart dijagram_projekti;
    @FXML
    private Label label_filter;
    @FXML
    private JFXButton btn_noviZadatak;
    @FXML
    private JFXButton btn_csv;
    @FXML
    private JFXButton btn_filtriraj;
    @FXML
    private JFXComboBox<String> cb_projekat;
    @FXML
    private JFXDatePicker dp_datumOd;
    @FXML
    private JFXDatePicker dp_datumDo;
    @FXML
    private JFXTextField tf_projekat;
    @FXML
    private JFXButton btn_kreirajProjekat;
    @FXML
    private JFXListView<Projekat> list_projekti;
    @FXML
    private StackPane stackPane;
    @FXML
    private Label label_napomena;
    @FXML
    private JFXToggleButton toggle_grupisi;
    @FXML
    private JFXComboBox<String> btn_tipDatoteke;
    @FXML
    private StackPane stack;
    @FXML
    private MenuItem m_izlaz;
    @FXML
    private MenuItem m_about;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicijalizujKolone();
        bazaPodataka = BazaPodataka.getInstance();
        bazaPodataka.vratiSveZadatke(listaZadataka);
        bazaPodataka.vratiSveProjekte(listaProjekata);
        popuniListuNazivaProjekata();
        popuniCBtipDatoteke();
        tabela.getItems().setAll(listaZadataka);
        list_projekti.getItems().setAll(listaProjekata);
        cb_projekat.getItems().setAll(stringListaProjekata);
        bazaPodataka.vratiBrojZadatakaPoProjektu(pieChartData, listaProjekata);
        dijagram_projekti.setData(pieChartData);
        JFXDepthManager.setDepth(label_napomena, 1);
    }    
    
    public void setBazaPodataka(BazaPodataka bazaPodataka){
        this.bazaPodataka = bazaPodataka;
    }
    
    public void setStage(Stage stage){
        this.stage = stage;
    }
    
    private void popuniListuNazivaProjekata(){
        listaProjekata.forEach((p) -> {
            stringListaProjekata.add(p.getNaziv());
        });
    }
    
    private void popuniCBtipDatoteke(){
        btn_tipDatoteke.getItems().add("PDF");
        btn_tipDatoteke.getItems().add("CSV");
        btn_tipDatoteke.getItems().add("XLS");
        btn_tipDatoteke.getItems().add("XLSX");
        btn_tipDatoteke.setValue("PDF");
    }
    
    private void inicijalizujKolone(){
        col_sifra.setCellValueFactory(new PropertyValueFactory<>("sifra"));
        col_projekat.setCellValueFactory(new PropertyValueFactory<>("projekat"));
        col_datum.setCellValueFactory(new PropertyValueFactory<>("datum"));
        col_vreme.setCellValueFactory(new PropertyValueFactory<>("vreme"));
        col_satnica.setCellValueFactory(new PropertyValueFactory<>("satnica"));
        col_zarada.setCellValueFactory(new PropertyValueFactory<>("zarada"));
    }
    
    private void ucitajDialog(String title, String content){
        
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Text(title));
        layout.setBody(new Text(content));
        
        JFXDialog dialog = new JFXDialog(stackPane, layout, JFXDialog.DialogTransition.CENTER);
        
        JFXButton btn_ok = new JFXButton("OK");
        btn_ok.setOnAction((event) -> {
            dialog.close();
        });
        btn_ok.getStyleClass().add("btn");
        
        layout.setActions(btn_ok);
        
        dialog.show();
        
    }
    
    @FXML
    public void addProjekat(ActionEvent event){
        String projekat = tf_projekat.getText();
        if(!projekat.isEmpty()){
            if(!bazaPodataka.projekatPostoji(projekat)){
                if(bazaPodataka.kreirajProjekat(projekat)){
                    listaProjekata.add(new Projekat(projekat));
                    cb_projekat.getItems().add(projekat);
                    list_projekti.getItems().add(new Projekat(projekat));
                    tf_projekat.setText("");
                    bazaPodataka.vratiBrojZadatakaPoProjektu(pieChartData, listaProjekata);
                    dijagram_projekti.setData(pieChartData);
                    ucitajDialog("Uspeh", "Novi projekat " + projekat + " \n"
                                + "je uspešno kreiran");
                }
                else{
                    ucitajDialog("Greška", "Projekat " + projekat + " nije kreiran. \n"
                                + "Javila se greška pri unosu u bazu podataka.");
                }
            }
            else{
                ucitajDialog("Greška", "Projekat pod imenom " + projekat + " već postoji.");
            }
        }
        else{
            ucitajDialog("Greška", "Molimo Vas da unesete naziv projekta.");
        }
    }
    
    @FXML
    public void filtriraj(ActionEvent event){
        LocalDate datumOd = dp_datumOd.getValue();
        LocalDate datumDo = dp_datumDo.getValue();
        
        String projekat = cb_projekat.getValue() != null ? cb_projekat.getValue() : "";
        
        StringBuilder filter = new StringBuilder();
        
        StringBuilder sb = new StringBuilder();
        
        if(toggle_grupisi.isSelected()){
            sb.append("SELECT "
                + "id, sifra, Sum(sati) AS sati, Sum(minuti) AS minuti, "
                + "satnica, datum, projekat FROM zadatak");
        }
        else{
            sb.append("SELECT "
                + "id, sifra, sati, minuti, satnica, "
                + "datum, projekat FROM zadatak");
        }
        if(projekat.isEmpty()){
            filter.append("Svi zadaci ");
            
            if(datumOd != null && datumDo != null){
                sb.append(" WHERE datum BETWEEN ");
                sb.append("'");
                sb.append(datumOd.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                sb.append(" AND ");
                sb.append("'");
                sb.append(datumDo.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                
                filter.append("od ");
                filter.append(datumOd.format(DateTimeFormatter.ISO_DATE));
                filter.append(" do ");
                filter.append(datumDo.format(DateTimeFormatter.ISO_DATE));
            }
            else if(datumOd != null && datumDo == null){
                sb.append(" WHERE datum >= '");
                sb.append(datumOd.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                
                filter.append("od ");
                filter.append(datumOd.format(DateTimeFormatter.ISO_DATE));
            }
            else if(datumOd == null && datumDo != null){
                sb.append(" WHERE datum <= '");
                sb.append(datumDo.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                
                filter.append("do ");
                filter.append(datumDo.format(DateTimeFormatter.ISO_DATE));
            }
        }
        else {
            filter.append("Projekat ");
            filter.append(projekat);
            
            sb.append(" WHERE projekat = ");
            sb.append("'");
            sb.append(projekat);
            sb.append("'");
            if(datumOd != null && datumDo != null){
                sb.append(" AND datum BETWEEN ");
                sb.append("'");
                sb.append(datumOd.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                sb.append(" AND ");
                sb.append("'");
                sb.append(datumDo.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                
                filter.append(" od ");
                filter.append(datumOd.format(DateTimeFormatter.ISO_DATE));
                filter.append(" do ");
                filter.append(datumDo.format(DateTimeFormatter.ISO_DATE));
            }
            else if(datumOd != null && datumDo == null){
                sb.append(" AND datum >= ");
                sb.append("'");
                sb.append(datumOd.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                
                filter.append(" od ");
                filter.append(datumOd.format(DateTimeFormatter.ISO_DATE));
            }
            else if(datumOd == null && datumDo != null){
                sb.append(" AND datum <= ");
                sb.append("'");
                sb.append(datumDo.format(DateTimeFormatter.ISO_DATE));
                sb.append("'");
                
                filter.append(" do ");
                filter.append(datumDo.format(DateTimeFormatter.ISO_DATE));
            }
        }
        if(toggle_grupisi.isSelected()){
            sb.append(" GROUP BY sifra,satnica");
            filter.append(" (grupisani)");
        }
        sb.append(" ORDER BY datum ASC");
        String upit = sb.toString();
        //System.out.println(upit);
        bazaPodataka.filtrirajZadatke(upit, listaZadataka);
        tabela.getItems().setAll(listaZadataka);
        label_filter.setText(filter.toString());
    }
    
    @FXML
    public void pretraga(){
        String trazeno = tf_pretraga.getText().toLowerCase();
        tabela.getItems().setAll( listaZadataka.stream()
            .filter(t -> t.getSifra().toLowerCase().contains(trazeno) 
                    || t.getProjekat().toLowerCase().contains(trazeno) 
                    || t.getDatum().toLowerCase().contains(trazeno))
            .collect(Collectors.toList()));  
    }
    
    private Notifications kreirajNotifikaciju(Pos position, String title, String text, Image image, Double seconds){
        Notifications notifications = Notifications.create()
                .title("Sačuvano")
                .text(text)
                .graphic(new ImageView(image))
                .hideAfter(Duration.seconds(seconds))
                .position(position)
                .darkStyle();

        return notifications;
    }
    
    void ucitajProzor(String location, String title, String stylesheet, String icon, boolean resizable){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(location));
            Parent parent = loader.load();
            Stage wstage = new Stage(StageStyle.DECORATED);
            wstage.setTitle(title);
            wstage.setScene(new Scene(parent));
            wstage.getScene().getStylesheets().add(stylesheet);
            wstage.getIcons().add(new Image(getClass().getResourceAsStream(icon)));
            wstage.setResizable(resizable);
            wstage.sizeToScene();
            wstage.initOwner(stackPane.getScene().getWindow());
            wstage.initModality(Modality.APPLICATION_MODAL);
            wstage.show();
            
            ZadatakController controller = (ZadatakController)loader.getController();
            controller.setProjekti(listaProjekata);
            controller.setZadaci(listaZadataka);
            controller.setButton(btn_filtriraj);
        } catch (IOException ex) {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    public void izveziPodatke(){
        String tipDatoteke = btn_tipDatoteke.getSelectionModel().getSelectedItem();
        Image image = new Image("/img/tick.png");
        String title = "Sačuvano";
        String text = "Datoteka sačuvana";
        Notifications notification = kreirajNotifikaciju(Pos.BOTTOM_RIGHT, title, text, image, 6.0);
        switch(tipDatoteke){
            case "PDF":
                Podaci.izveziPDF(label_filter.getText(), stage, listaZadataka, notification);
                break;
            case "CSV":
                Podaci.izveziCSV(label_filter.getText(), stage, listaZadataka, notification);
                break;
            case "XLS":
                Podaci.izveziXLS(label_filter.getText(), stage, listaZadataka, notification);
                break;
            case "XLSX":
                Podaci.izveziXLSX(label_filter.getText(), stage, listaZadataka, notification);
                break;
        }
    }
    
    @FXML
    public void prozorNoviZadatak(){
        ucitajProzor("/inovator/zadatak/ZadatakFXML.fxml", "Novi zadatak", "/inovator/zadatak/zadatak.css", "/img/tasks.png", false);
    }
    
    @FXML
    public void izlaz(){
        System.exit(0);
    }
}

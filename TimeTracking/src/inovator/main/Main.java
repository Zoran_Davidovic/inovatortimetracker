package inovator.main;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/inovator/main/MainFXML.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/inovator/main/mainfxml.css");
        primaryStage.setTitle("Inovator");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/tasks.png")));
        primaryStage.setScene(scene);
        //primaryStage.setResizable(false);
        primaryStage.setMinHeight(740);
        primaryStage.setMinWidth(1010);
        primaryStage.sizeToScene();
        primaryStage.show();
        
        MainFXMLController controller = (MainFXMLController)loader.getController();
        controller.setStage(primaryStage);
        
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}

package inovator.main;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import modeli.Zadatak;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.controlsfx.control.Notifications;

public class Podaci {

    public Podaci() {
    }
    
    public static void izveziPDF(String filter, Stage stage, ObservableList<Zadatak> listaZadataka, Notifications notification){
        FileChooser fc = new FileChooser();
        fc.setInitialFileName(filter+".pdf");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        fc.getExtensionFilters().add(extFilter);
        fc.setTitle("Izvezi podatke kao PDF");
        File file = fc.showSaveDialog(stage);
        if (file != null) {
           
            try (OutputStream os = new FileOutputStream(file)) {
                os.write(239);
                os.write(187);
                os.write(191);
                    
                Document pdfDocument = new Document(PageSize.A4);
                PdfWriter.getInstance(pdfDocument, os);
                pdfDocument.open();

                PdfPTable table = new PdfPTable(6);
                PdfPCell naslov = new PdfPCell(new Paragraph(filter));
                naslov.setColspan(6);
                naslov.setHorizontalAlignment(Element.ALIGN_CENTER);
                naslov.setPaddingTop(5);
                naslov.setPaddingBottom(5);
                naslov.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(naslov);
                
                PdfPCell c_sifra = new PdfPCell(new Paragraph("Šifra"));
                c_sifra.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c_sifra.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c_sifra);
                
                PdfPCell c_projekat = new PdfPCell(new Paragraph("Projekat"));
                c_projekat.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c_projekat.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c_projekat);
                
                PdfPCell c_datum = new PdfPCell(new Paragraph("Datum"));
                c_datum.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c_datum.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c_datum);
                
                PdfPCell c_vreme = new PdfPCell(new Paragraph("Vreme"));
                c_vreme.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c_vreme.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c_vreme);
                
                PdfPCell c_satnica = new PdfPCell(new Paragraph("Satnica"));
                c_satnica.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c_satnica.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c_satnica);
                
                PdfPCell c_zarada = new PdfPCell(new Paragraph("Zarada"));
                c_zarada.setBackgroundColor(BaseColor.LIGHT_GRAY);
                c_zarada.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(c_zarada);
                
                table.setHeaderRows(2);
                
                int sati = 0;
                int minuti = 0;
                double zarada = 0.0;
                Zadatak z;

                
                for (Iterator<Zadatak> it = listaZadataka.iterator(); it.hasNext();) {
                    z = it.next();
                    sati += z.getSati();
                    minuti += z.getMinuti();
                    zarada += z.getZarada();

                    PdfPCell[] nizCelija = new PdfPCell[6];
                    nizCelija[0] = new PdfPCell(Phrase.getInstance(z.getSifra()));
                    nizCelija[1] = new PdfPCell(Phrase.getInstance(z.getProjekat()));
                    nizCelija[2] = new PdfPCell(Phrase.getInstance(z.getDatum()));
                    nizCelija[3] = new PdfPCell(Phrase.getInstance(z.getVreme()));
                    nizCelija[4] = new PdfPCell(Phrase.getInstance(String.valueOf(z.getSatnica())));
                    nizCelija[5] = new PdfPCell(Phrase.getInstance(String.valueOf(z.getZarada())));

                    PdfPRow red = new PdfPRow(nizCelija);
                    red.getCells()[4].setHorizontalAlignment(Element.ALIGN_RIGHT);
                    red.getCells()[5].setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.getRows().add(red);
                }

                sati += (minuti/60);
                minuti %= 60;
                String vreme = sati+"h"+minuti+"m";
                
                zarada = zarada*100;
                zarada = Math.round(zarada);
                zarada = zarada/100;
                
                PdfPCell[] nizCelija = new PdfPCell[6];
                PdfPCell celijaUkupno = new PdfPCell(Phrase.getInstance("Ukupno"));
                celijaUkupno.setBackgroundColor(BaseColor.YELLOW);
                celijaUkupno.setColspan(3);
                nizCelija[0] = celijaUkupno;
                PdfPCell celijaVreme = new PdfPCell(Phrase.getInstance(vreme));
                celijaVreme.setBackgroundColor(BaseColor.YELLOW);
                celijaVreme.setColspan(2);
                nizCelija[3] = celijaVreme;
                PdfPCell celijaZarada = new PdfPCell(Phrase.getInstance(String.valueOf(zarada)));
                celijaZarada.setBackgroundColor(BaseColor.YELLOW);
                celijaZarada.setHorizontalAlignment(Element.ALIGN_RIGHT);
                nizCelija[5] = celijaZarada;
                table.getRows().add(new PdfPRow(nizCelija));
                pdfDocument.add(table);
                pdfDocument.close();
                
                Image image = new Image("/img/tick.png");
                String title = "Sačuvano";
                String text = "Datoteka "+file.getName()+" sačuvana";
                notification.onAction((ActionEvent event) -> {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException ex) {
                        Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                notification.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException | DocumentException ex) {
                Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void izveziXLS(String filter, Stage stage, ObservableList<Zadatak> listaZadataka, Notifications notification){
        FileChooser fc = new FileChooser();
        fc.setInitialFileName(filter+".xls");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel XLS files (*.xls)", "*.xls");
        fc.getExtensionFilters().add(extFilter);
        fc.setTitle("Izvezi podatke kao Excel (XLS) datoteku");
        File file = fc.showSaveDialog(stage);
        if (file != null) {
           
            try (OutputStream os = new FileOutputStream(file)) {
                    
                HSSFWorkbook workbook = new HSSFWorkbook();
                HSSFSheet sheet = workbook.createSheet(filter);
                HSSFRow header = sheet.createRow(0);
                header.createCell(0).setCellValue("Šifra");
                header.createCell(1).setCellValue("Projekat");
                header.createCell(2).setCellValue("Datum");
                header.createCell(3).setCellValue("Vreme");
                header.createCell(4).setCellValue("Satnica");
                header.createCell(5).setCellValue("Projekat");
                
                int sati = 0;
                int minuti = 0;
                double zarada = 0.0;
                Zadatak z;

                
                for (int i = 0; i < listaZadataka.size(); i++) {
                    HSSFRow row = sheet.createRow(i+1);
                    sati += listaZadataka.get(i).getSati();
                    minuti += listaZadataka.get(i).getMinuti();
                    zarada += listaZadataka.get(i).getZarada();
                    
                    row.createCell(0).setCellValue(listaZadataka.get(i).getSifra());
                    row.createCell(1).setCellValue(listaZadataka.get(i).getProjekat());
                    row.createCell(2).setCellValue(listaZadataka.get(i).getDatum());
                    row.createCell(3).setCellValue(listaZadataka.get(i).getVreme());
                    row.createCell(4).setCellValue(listaZadataka.get(i).getSatnica());
                    row.createCell(5).setCellValue(listaZadataka.get(i).getZarada());
                }

                sati += (minuti/60);
                minuti %= 60;
                String vreme = sati+"h"+minuti+"m";
                
                zarada = zarada*100;
                zarada = Math.round(zarada);
                zarada = zarada/100;
                
                HSSFRow footer = sheet.createRow(listaZadataka.size()+1);
                footer.createCell(0).setCellValue("Ukupno");
                footer.createCell(3).setCellValue(vreme);
                footer.createCell(5).setCellValue(zarada);
                
                workbook.write(os);
                workbook.close();
                
                notification.onAction((ActionEvent event) -> {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException ex) {
                        Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                notification.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void izveziXLSX(String filter, Stage stage, ObservableList<Zadatak> listaZadataka, Notifications notification){
        FileChooser fc = new FileChooser();
        fc.setInitialFileName(filter+".xlsx");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel XLSX files (*.xlsx)", "*.xlsx");
        fc.getExtensionFilters().add(extFilter);
        fc.setTitle("Izvezi podatke kao Excel (XLSX) datoteku");
        File file = fc.showSaveDialog(stage);
        if (file != null) {
           
            try (OutputStream os = new FileOutputStream(file)) {
                    
                XSSFWorkbook workbook = new XSSFWorkbook();
                XSSFSheet sheet = workbook.createSheet(filter);
                XSSFRow header = sheet.createRow(0);
                header.createCell(0).setCellValue("Šifra");
                header.createCell(1).setCellValue("Projekat");
                header.createCell(2).setCellValue("Datum");
                header.createCell(3).setCellValue("Vreme");
                header.createCell(4).setCellValue("Satnica");
                header.createCell(5).setCellValue("Projekat");
                
                int sati = 0;
                int minuti = 0;
                double zarada = 0.0;
                Zadatak z;

                
                for (int i = 0; i < listaZadataka.size(); i++) {
                    XSSFRow row = sheet.createRow(i+1);
                    
                    sati += listaZadataka.get(i).getSati();
                    minuti += listaZadataka.get(i).getMinuti();
                    zarada += listaZadataka.get(i).getZarada();
                    
                    row.createCell(0).setCellValue(listaZadataka.get(i).getSifra());
                    row.createCell(1).setCellValue(listaZadataka.get(i).getProjekat());
                    row.createCell(2).setCellValue(listaZadataka.get(i).getDatum());
                    row.createCell(3).setCellValue(listaZadataka.get(i).getVreme());
                    row.createCell(4).setCellValue(listaZadataka.get(i).getSatnica());
                    row.createCell(5).setCellValue(listaZadataka.get(i).getZarada());
                }

                sati += (minuti/60);
                minuti %= 60;
                String vreme = sati+"h"+minuti+"m";
                
                zarada = zarada*100;
                zarada = Math.round(zarada);
                zarada = zarada/100;
                
                XSSFRow footer = sheet.createRow(listaZadataka.size()+1);
                footer.createCell(0).setCellValue("Ukupno");
                footer.createCell(3).setCellValue(vreme);
                footer.createCell(5).setCellValue(zarada);
                
                workbook.write(os);
                workbook.close();
                
                notification.onAction((ActionEvent event) -> {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException ex) {
                        Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                notification.show();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void izveziCSV(String filter, Stage stage, ObservableList<Zadatak> listaZadataka, Notifications notification){
        FileChooser fc = new FileChooser();
        fc.setInitialFileName(filter+".csv");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fc.getExtensionFilters().add(extFilter);
        fc.setTitle("Izvezi podatke kao CSV");
        File file = fc.showSaveDialog(stage);
        if (file != null) {
           
            try (OutputStream os = new FileOutputStream(file)) {
                os.write(239);
                os.write(187);
                os.write(191);
                try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, "UTF-8"))) {
                    
                    //upisujemo nazive kolona
                    pw.println("Šifra,Projekat,Datum,Vreme,Satnica,Zarada");
                    pw.flush();
                    
                    int sati = 0;
                    int minuti = 0;
                    double zarada = 0.0;
                    Zadatak z;
                    for (Iterator<Zadatak> it = listaZadataka.iterator(); it.hasNext();) {
                        z = it.next();
                        sati += z.getSati();
                        minuti += z.getMinuti();
                        zarada += z.getZarada();
                        pw.println(z.getSifra() + "," + z.getProjekat() + "," +
                                z.getDatum() + "," + z.getVreme() + "," +
                                z.getSatnica() + "," + z.getZarada());
                        pw.flush();
                    }
                    
                    pw.println();
                    pw.flush();
                    
                    sati += (minuti/60);
                    minuti %= 60;
                    String vreme = sati+"h"+minuti+"m";
                    
                    pw.println("Ukupno,,,"+vreme+",,"+zarada);
                    pw.flush();
                    
                    Image image = new Image("/img/tick.png");
                    String title = "Sačuvano";
                    String text = "Datoteka "+file.getName()+" sačuvana";
                    notification.onAction((ActionEvent event) -> {
                        try {
                            Desktop.getDesktop().open(file);
                        } catch (IOException ex) {
                            Logger.getLogger(Podaci.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                    notification.show();
                }
            }catch(IOException ex){
                
            }
        }
    }
}

package inovator.zadatak;

import com.jfoenix.controls.*;
import database.BazaPodataka;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Function;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import modeli.Projekat;
import modeli.Zadatak;

public class ZadatakController implements Initializable {
    
    BazaPodataka bazaPodataka;
    Button b;
    
    ObservableList<Projekat> listaProjekata;
    ObservableList<Zadatak> listaZadataka;
    
    @FXML
    private JFXButton save;
    @FXML
    private JFXButton cancel;
    @FXML
    private AnchorPane rootPane;
    @FXML
    private JFXTextField new_sifra;
    @FXML
    private JFXComboBox<String> new_projekat;
    @FXML
    private JFXDatePicker new_datum;
    @FXML
    private JFXTextField new_sati;
    @FXML
    private JFXTextField new_minuti;
    @FXML
    private JFXTextField new_satnica;
    @FXML
    private StackPane stack;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bazaPodataka = BazaPodataka.getInstance();
    }
    
    public void setProjekti(ObservableList<Projekat> projekti){
        this.listaProjekata = projekti;
        popuniComboBoxProjekti();
    }
    
    public void setZadaci(ObservableList<Zadatak> zadaci){
        this.listaZadataka = zadaci;
    }
    
    public void setButton(Button filter){
        this.b = filter;
    }
    
    
    private void popuniComboBoxProjekti(){
        listaProjekata.forEach((p) -> {
            new_projekat.getItems().add(p.getNaziv());
        });
        
    }
    
    private void alert(String content, String header, String title){
        Alert alert = new Alert(Alert.AlertType.ERROR, content, ButtonType.OK);
        alert.setHeaderText(header);
        alert.setTitle(title);
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(getClass().getResourceAsStream("/img/tasks.png")));
        alert.showAndWait();
    }
    
    @FXML
    private void ucitajDialog(String title, String content){
        
        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Text(title));
        layout.setBody(new Text(content));
        
        JFXDialog dialog = new JFXDialog(stack, layout, JFXDialog.DialogTransition.CENTER);
        
        JFXButton btn_ok = new JFXButton("OK");
        btn_ok.setOnAction((event) -> {
            dialog.close();
        });
        btn_ok.getStyleClass().add("btn");
        
        layout.setActions(btn_ok);
        
        dialog.show();
        
    }
    
    @FXML
    private void addZadatak(ActionEvent event) {
        String sifra = new_sifra.getText().trim();
        String projekat = new_projekat.getValue();
        String datum = (new_datum.getValue() != null) ? new_datum.getValue().toString() : "";
        String sati = new_sati.getText().trim();
        String minuti = new_minuti.getText().trim();
        String satnica = new_satnica.getText().trim();
        
        if(sifra.isEmpty() || sati.isEmpty() || minuti.isEmpty() 
                || satnica.isEmpty() || projekat.isEmpty() || datum.isEmpty()){
            ucitajDialog("Greška", "Sva polja su obavezna.\n"
                    + "Molimo Vas da popunite sva polja\n"
                    + "pre nego što kliknete na dugme 'Sačuvaj')");
        }
        else{
            if(bazaPodataka.dodajZadatak(sifra, projekat, datum, Integer.parseInt(sati), Integer.parseInt(minuti), Double.parseDouble(satnica))){
                ucitajDialog("Uspeh", "Zadatak sa šifrom "+sifra+" \n"
                    + "iz projekta "+projekat+" \n"
                    + "je uspešno upisan u bazu podataka.");
                listaZadataka.add(new Zadatak(sifra, Integer.parseInt(sati), Integer.parseInt(minuti), Double.parseDouble(satnica), datum, projekat));
                b.fire();
            }
            else{
                ucitajDialog("Greška", "Zadatak sa šifrom "+sifra+" \n"
                    + "iz projekta "+projekat+" \n"
                    + "je nije upisan u bazu podataka.");
            }
        }
    }
    
    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage)rootPane.getScene().getWindow();
        stage.close();
    }
}
